=== Plugin Name ===
Contributors: marceljm
Donate link: https://donorbox.org/fifu
Tags: featured, image, url, woocommerce, thumbnail
Requires at least: 5.3
Tested up to: 5.6
Stable tag: 3.5.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Use an external image as featured image of a post or WooCommerce product. Includes image search, video, social tags, SEO, lazy load, gallery, automation etc.

== Description ==

### WordPress plugin for external featured image

Since 2015 FIFU has helped thousands of websites worldwide to save money on storage, processing and copyright.

If you are tired of wasting time and resources with thumbnail regeneration, image optimization and never-ending imports, this plugin is for you.

#### FEATURED IMAGE
Use an external image as featured image of your post, page or custom post type.

* External featured image
* Unsplash image search
* Default featured image
* Hide featured media
* Featured image in content
* Elementor widget
* Auto set image title
* Save image dimensions
* Featured image column
* **[Premium]** Giphy image search
* **[Premium]** Unsplash image size
* **[Premium]** Same height
* **[Premium]** Hover effects
* **[Premium]** Replace not found image
* **[Premium]** Image validation

#### AUTOMATIC FEATURED MEDIA

* Auto set featured image/video using img/iframe tag from post content
* **[Premium]** Auto set featured image using post title and search engine
* **[Premium]** Auto set featured image using ISBN and books API

#### PERFORMANCE

* CDN + optimized thumbnails
* Lazy load
* **[Premium]** Flickr thumbnails

#### SOCIAL

* Social tags
* **[Premium]** Media RSS tags

#### AUTOMATION

* WP-CLI integration
* **[Premium]** WP All Import add-on
* **[Premium]** WooCommerce import
* **[Premium]** WP REST API
* **[Premium]** WooCommerce REST API
* **[Premium]** Schedule metadata generation

#### WOOCOMMERCE

* External product image
* Lightbox and zoom
* Category image on grid
* **[Premium]** External image gallery
* **[Premium]** External video gallery
* **[Premium]** Auto set category images
* **[Premium]** Variable product
* **[Premium]** Variation image
* **[Premium]** Variation image gallery

#### FEATURED VIDEO
Supports videos from YouTube, Vimeo, Imgur, 9GAG, Cloudinary, Tumblr and Publitio.

* **[Premium]** Featured video
* **[Premium]** Video thumbnail
* **[Premium]** Play button
* **[Premium]** Minimum width
* **[Premium]** Black background
* **[Premium]** Mouseover autoplay
* **[Premium]** Autoplay
* **[Premium]** Loop
* **[Premium]** Mute
* **[Premium]** Background video
* **[Premium]** Related videos
* **[Premium]** Gallery icon

#### OTHERS

* **[Premium]** Featured slider 
* **[Premium]** Featured shortcode 

#### INTEGRATION FUNCTION FOR DEVELOPERS

* fifu_dev_set_image(post_id, image_url)
* **[Premium]** fifu_dev_set_image_list(post_id, image_url_list)

#### LINKS

* **<a href="https://fifu.app/">Featured Image from URL Premium</a>**	
* **<a href="https://chrome.google.com/webstore/detail/fifu-scraper/pccimcccbkdeeadhejdmnffmllpicola">Google Chrome extension</a>**


== Installation ==

### INSTALL FIFU FROM WITHIN WORDPRESS

1. Visit the plugins page within your dashboard and select 'Add New';
1. Search for 'Featured Image from URL';
1. Activate FIFU from your Plugins page;

### INSTALL FIFU MANUALLY

1. Upload the 'featured-image-from-url' folder to the /wp-content/plugins/ directory;
1. Activate the FIFU plugin through the 'Plugins' menu in WordPress;


== Frequently Asked Questions ==

= Where is the external featured image box? =

* Next to the regular featured image box, in the post editor.

= Why isn't preview button working? =

* Your image URL is invalid. Take a look at FIFU Settings > Getting started.

= Does FIFU save the images in the media library? =

* Never.

= Is any action necessary before removing FIFU?

* Access settings and clean the metadata.

= What's the metadata created by FIFU?

* Database registers that help WordPress components to work with the external images. FIFU can generate the metadata of ~30,000 image URLs per minute.

= What are the disadvantages of the external images?

* No image optimization or thumbnails. You can fix that with Jetpack plugin (performance settings).

= What are the advantages of the external images?

* You save money on storage, processing and copyright. And you can have extremely fast import processes.


== Screenshots ==

1. Just insert the image address or some keywords and click on preview button

2. If the image URL is correct, the image will be shown

3. Featured video

4. Featured slider

5. Featured image column

6. External featured media on home

7. External featured image on post

8. Featured video on post

9. Featured slider on post

10. Many settings

11. External image gallery

12. External video gallery

13. External featured media on WooCommerce shop

14. External featured media on cart

15. External image gallery

16. External featured image on lightbox

17. Zoom

18. Video gallery

19. Featured video on lightbox

20. Fullscreen


== Changelog ==

= 3.5.0 =
* Improvement: WP-CLI integration is complete (90 commands).

= 3.4.9 =
* Improvement: 13 new WP-CLI commands added; fix: missing images after WordPress core updates (affects images with long URLs); bug fix: images not being displayed for Anywhere Elementor plugin (and maybe other components).

= 3.4.8 =
* Notice: for users with Instagram images; improvement: 20 new WP-CLI commands added; fix: Unsplash random image.

= others =
* [more](https://fifu.app/changelog/)


== Upgrade Notice ==

= 3.5.0 =
* Improvement: WP-CLI integration is complete (90 commands).
