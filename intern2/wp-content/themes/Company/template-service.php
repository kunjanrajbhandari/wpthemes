<?php 
get_header();
/* Template Name: service */
?>


<?php get_header();?>


<main id="main">

<!-- ======= Breadcrumbs ======= -->
<section id="breadcrumbs" class="breadcrumbs">
  <div class="container">

    <div class="d-flex justify-content-between align-items-center">
      <h2>Services</h2>
      <ol>
        <li><a href="index.html">Home</a></li>
        <li>Services</li>
      </ol>
    </div>

  </div>
</section><!-- End Breadcrumbs -->

<!-- ======= Services Section ======= -->
<?php $service = get_field('service_type');?>
<section id="services" class="services section-bg">
  <div class="container" data-aos="fade-up">

    <div class="row">
    
      <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
        <div class="icon-box iconbox-blue">
          <div class="icon">
            <p?> <?php echo "<img src='".$service['image']['url']."'>";?></p>  <!-- for advanced custom field  -->
            
          </div>
           
          <h4><a href=""><?php echo  $service['service_title'];?></a></h4>
          
          <p><?php echo $service['service_description'];?></p>
        </div>
      </div>
      <?php $service2 = get_field('service_type2');?>
      <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
      <div class="icon-box iconbox-blue">
          <div class="icon">
            <p?> <?php echo "<img src='".$service2['image']['url']."'>";?></p>  <!-- for advanced custom field  -->
            
          </div>
           
          <h4><a href=""><?php echo  $service2['service_title'];?></a></h4>
          
          <p><?php echo $service2['service_description'];?></p>
        </div>
      </div>
      <?php $service3 = get_field('service_type3');?>
      <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="300">
        <div class="icon-box iconbox-pink">
        <div class="icon">
          <p?> <?php echo "<img src='".$service3['image']['url']."'>";?></p>  <!-- for advanced custom field  -->
            
          </div>
           
          <h4><a href=""><?php echo  $service3['service_title'];?></a></h4>
          
          <p><?php echo $service3['service_description'];?></p>
        </div>
      </div>
      <?php $service4 = get_field('service_type4');?>
      <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
        <div class="icon-box iconbox-yellow">
        <div class="icon">
          <p?> <?php echo "<img src='".$service4['image']['url']."'>";?></p>  <!-- for advanced custom field  -->
            
          </div>
           
          <h4><a href=""><?php echo  $service4['service_title'];?></a></h4>
          
          <p><?php echo $service4['service_description'];?></p>
        </div>
      </div>
      <?php $service5 = get_field('service_type5');?>
      <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="200">
        <div class="icon-box iconbox-red">
        <div class="icon">
          <p?> <?php echo "<img src='".$service5['image']['url']."'>";?></p>  <!-- for advanced custom field  -->
            
          </div>
           
          <h4><a href=""><?php echo  $service5['service_title'];?></a></h4>
          
          <p><?php echo $service5['service_description'];?></p>
        </div>
      </div>
      <?php $service6 = get_field('service_type6');?>
      <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="300">
        <div class="icon-box iconbox-teal">
        <div class="icon">
          <p?> <?php echo "<img src='".$service6['image']['url']."'>";?></p>  <!-- for advanced custom field  -->
            
          </div>
           
          <h4><a href=""><?php echo  $service6['service_title'];?></a></h4>
          
          <p><?php echo $service6['service_description'];?></p>
        </div>
      </div>

    </div>

  </div>
</section><!-- End Services Section -->

<!-- ======= Features Section ======= -->
<section id="features" class="features">
  <div class="container" data-aos="fade-up">

    <div class="section-title">
      <h2>Features</h2>
      <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
    </div>

    <div class="row">
      <div class="col-lg-3 col-md-4">
        <div class="icon-box">
          <i class="ri-store-line" style="color: #ffbb2c;"></i>
          <h3><a href="">Lorem Ipsum</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
        <div class="icon-box">
          <i class="ri-bar-chart-box-line" style="color: #5578ff;"></i>
          <h3><a href="">Dolor Sitema</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4 mt-md-0">
        <div class="icon-box">
          <i class="ri-calendar-todo-line" style="color: #e80368;"></i>
          <h3><a href="">Sed perspiciatis</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4 mt-lg-0">
        <div class="icon-box">
          <i class="ri-paint-brush-line" style="color: #e361ff;"></i>
          <h3><a href="">Magni Dolores</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4">
        <div class="icon-box">
          <i class="ri-database-2-line" style="color: #47aeff;"></i>
          <h3><a href="">Nemo Enim</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4">
        <div class="icon-box">
          <i class="ri-gradienter-line" style="color: #ffa76e;"></i>
          <h3><a href="">Eiusmod Tempor</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4">
        <div class="icon-box">
          <i class="ri-file-list-3-line" style="color: #11dbcf;"></i>
          <h3><a href="">Midela Teren</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4">
        <div class="icon-box">
          <i class="ri-price-tag-2-line" style="color: #4233ff;"></i>
          <h3><a href="">Pira Neve</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4">
        <div class="icon-box">
          <i class="ri-anchor-line" style="color: #b2904f;"></i>
          <h3><a href="">Dirada Pack</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4">
        <div class="icon-box">
          <i class="ri-disc-line" style="color: #b20969;"></i>
          <h3><a href="">Moton Ideal</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4">
        <div class="icon-box">
          <i class="ri-base-station-line" style="color: #ff5828;"></i>
          <h3><a href="">Verdo Park</a></h3>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mt-4">
        <div class="icon-box">
          <i class="ri-fingerprint-line" style="color: #29cc61;"></i>
          <h3><a href="">Flavor Nivelanda</a></h3>
        </div>
      </div>
    </div>

  </div>
</section><!-- End Features Section -->

</main><!-- End #main -->



<?php get_footer();?>