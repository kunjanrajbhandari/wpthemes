<?php 
function load_stylesheets(){

  
    wp_register_style('favicon', get_template_directory_uri().'/assets/img/favicon.png', array(), 1, 'all');
    wp_enqueue_style('favicon');

    wp_register_style('icon', get_template_directory_uri().'/assets/img/apple-touch-icon.png', array(), 1, 'all');
    wp_enqueue_style('icon');



    wp_register_style('bootstrap', get_template_directory_uri().'/assets/vendor/bootstrap/css/bootstrap.min.css', array(), 1, 'all');
    wp_enqueue_style('bootstrap');
 
    wp_register_style('icofont', get_template_directory_uri().'/assets/vendor/icofont/icofont.min.css', array(), 1, 'all');
    wp_enqueue_style('icofont');

    wp_register_style('boxicons', get_template_directory_uri().'/assets/vendor/boxicons/css/boxicons.min.css', array(), 1, 'all');
    wp_enqueue_style('boxicons');

    wp_register_style('animate', get_template_directory_uri().'/assets/vendor/animate.css/animate.min.css', array(), 1, 'all');
    wp_enqueue_style('animate');

    wp_register_style('venobox', get_template_directory_uri().'/assets/vendor/venobox/venobox.css', array(), 1, 'all');
    wp_enqueue_style('venobox');

    wp_register_style('carousel', get_template_directory_uri().'/assets/vendor/owl.carousel/assets/owl.carousel.min.css', array(), 1, 'all');
    wp_enqueue_style('carousel');

    wp_register_style('aos', get_template_directory_uri().'/assets/vendor/aos/aos.css', array(), 1, 'all');
    wp_enqueue_style('aos');

    wp_register_style('remixicon', get_template_directory_uri().'/assets/vendor/remixicon/remixicon.css', array(), 1, 'all');
    wp_enqueue_style('remixicon');
    
    wp_register_style('style', get_template_directory_uri().'/assets/css/style.css', array(), 1, 'all');
    wp_enqueue_style('style');
    
}
add_action('wp_enqueue_scripts','load_stylesheets');



//load scripts
function addjs(){
    wp_register_script('jquery', get_template_directory_uri(). '/assets/vendor/jquery/jquery.min.js', array(), 1,1,1);
    wp_enqueue_script('jquery');

    wp_register_script('bundle', get_template_directory_uri(). '/assets/vendor/bootstrap/js/bootstrap.bundle.min.js', array(), 1,1,1);
    wp_enqueue_script('bundle');

    wp_register_script('easing', get_template_directory_uri(). '/assets/vendor/jquery.easing/jquery.easing.min.js', array(), 1,1,1);
    wp_enqueue_script('easing');

    wp_register_script('validate', get_template_directory_uri(). '/assets/vendor/php-email-form/validate.js', array(), 1,1,1);
    wp_enqueue_script('validate');

    wp_register_script('sticky', get_template_directory_uri(). '/assets/vendor/jquery-sticky/jquery.sticky.js', array(), 1,1,1);
    wp_enqueue_script('sticky');

    wp_register_script('isotope', get_template_directory_uri(). '/assets/vendor/isotope-layout/isotope.pkgd.min.js', array(), 1,1,1);
    wp_enqueue_script('isotope');

    wp_register_script('venobox', get_template_directory_uri(). '/assets/vendor/venobox/venobox.min.js', array(), 1,1,1);
    wp_enqueue_script('venobox');

    wp_register_script('waypoints', get_template_directory_uri(). '/assets/vendor/waypoints/jquery.waypoints.min.js', array(), 1,1,1);
    wp_enqueue_script('waypoints');

    wp_register_script('carousel', get_template_directory_uri(). '/assets/vendor/owl.carousel/owl.carousel.min.js', array(), 1,1,1);
    wp_enqueue_script('carousel');

    wp_register_script('aos', get_template_directory_uri(). '/assets/vendor/aos/aos.js', array(), 1,1,1);
    wp_enqueue_script('aos');

    wp_register_script('main', get_template_directory_uri(). '/assets/js/main.js', array(), 1,1,1);
    wp_enqueue_script('main');
}
add_action('wp_enqueue_scripts','addjs');


//theme support

add_theme_support('post-thumbnails');
// add_theme_support('menus');
 



add_theme_support('post-thumbnails');
function register_menu(){
    register_nav_menus(
        array(
            'primary-menu'=> 'Primary Menu',
            'footer-menu' => 'Footer Menu'
        )
        );
}
add_action("init","register_menu");
function selected_class($classes,$item){
    if(in_array('current-menu-item',$classes)){
        $classes[]='selected';
    }
    return $classes;
}
add_filter('nav_menu_css_class','selected_class',10,2);
