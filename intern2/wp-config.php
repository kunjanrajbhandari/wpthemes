<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'intern2' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'K1<R{/Ms!DT)Vh^[3d7) l}dCgO]yVR$y8QZqe5%8XfLUL4dnYVP{Fpzj(Hj1k/S' );
define( 'SECURE_AUTH_KEY',  'X=9y$]/xT>jjE_G8=l>9[2XNi>&Jn?u5V|Frm(1P%vnYP7<mQrTbl[D1XayCe^N3' );
define( 'LOGGED_IN_KEY',    '[+f(|S|PEP s~1+qIA1+wS!;[a[u*.{lu;}$3|StL0S?}:R|n^7[8q~kFc|>CQ/j' );
define( 'NONCE_KEY',        'YTs]Ut{[ NO-YOmc+=]pc`{#V:[`~7ZMU+~s7~;}XF.mxnxx:U_3YH%x?F6,is$I' );
define( 'AUTH_SALT',        'm4F6US-aD_jgDL$|ElpYK@5RKAS}st>#[_b)P>b2T|#$dlp &N$G=9xz0.Lue/jR' );
define( 'SECURE_AUTH_SALT', 'uk(OB @e8&9V-@u}o{xy{t,<z$CrRGF0(bS([z?tzZm!Wp3vO$D{!v& evEfv6N>' );
define( 'LOGGED_IN_SALT',   'Cs)Z7{@drnTS,?N3Op`&QFSH?9^v|EB>CV>OeKYU<$vS[q% =TVk5Ga*0v+Kn-B(' );
define( 'NONCE_SALT',       '@8d|NWrzD3ez&RMeX~Q^F>[e:Rqa-[sgM7a#*x*J$v?6g<ug|y?^33-M@.kYVH`F' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
